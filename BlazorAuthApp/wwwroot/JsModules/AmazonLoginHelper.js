window.AmazonLoginHelper = {
    runLoginProcess: function(dotNetHelper, state) {
        const options = {
            scope: 'profile',
            response_type: 'code',
            state: state
        };

        amazon.Login.authorize(options, function (response) {
            if (response.error) {
                console.error(response.error);
                dotNetHelper.invokeMethodAsync('NotifyLoginFailed');
                return;
            }
            
            dotNetHelper.invokeMethodAsync('NotifyLoginCompletion', state, response.code);
        });
    }
}
