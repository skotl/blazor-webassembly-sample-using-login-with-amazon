using System.Security.Claims;
using Microsoft.AspNetCore.Components.Authorization;

namespace BlazorAuthApp.Auth;

public class CustomAuthenticationProvider : AuthenticationStateProvider
{
    private readonly IBearerStorage _bearerStorage;

    public CustomAuthenticationProvider(IBearerStorage bearerStorage)
    {
        _bearerStorage = bearerStorage;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        var token = await _bearerStorage.GetAsync();
        if (string.IsNullOrEmpty(token))
        {
            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));
        }

        var userClaimPrincipal =
            new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), "auth"));

        return new AuthenticationState(userClaimPrincipal);
    }

    public void Notify()
    {
        NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
    }
}