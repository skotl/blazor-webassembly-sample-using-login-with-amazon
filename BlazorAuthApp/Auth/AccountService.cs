using BlazorAuthApp.Exceptions;
using Microsoft.AspNetCore.Components.Authorization;

namespace BlazorAuthApp.Auth;

public class AccountService : IAccountService
{
    private readonly IBearerStorage _bearerStorage;
    private readonly AuthenticationStateProvider _customAuthenticationProvider;
    private readonly IExternalLwaService _externalLwaService;

    public AccountService(
        IBearerStorage bearerStorage,
        AuthenticationStateProvider customAuthenticationProvider,
        IExternalLwaService externalLwaService
        )
    {
        _bearerStorage = bearerStorage;
        _customAuthenticationProvider = customAuthenticationProvider ??
                                        throw new ArgumentNullException(nameof(customAuthenticationProvider));
        _externalLwaService = externalLwaService;
    }

    public async Task<bool> CompleteLogin(string state, string code)
    {
        var token = await _externalLwaService.ExchangeLwaCodeAsync(state, code);
        if (string.IsNullOrWhiteSpace(token))
            throw new LwaException("Unable to fetch bearer token");

        await _bearerStorage.StoreAsync(token);
        (_customAuthenticationProvider as CustomAuthenticationProvider)?.Notify();
        
        return true;
    }

    public async Task<bool> LogoutAsync()
    {
        await _externalLwaService.LogoutAsync();
        await _bearerStorage.ClearAsync();
        
        (_customAuthenticationProvider as CustomAuthenticationProvider)?.Notify();
        return true;
    }
}