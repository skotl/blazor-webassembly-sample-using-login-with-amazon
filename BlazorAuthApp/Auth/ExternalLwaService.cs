using System.Net.Http.Json;
using BlazorAuthApp.Exceptions;
using BlazorAuthApp.Model;
using Microsoft.Extensions.Options;

namespace BlazorAuthApp.Auth;

public class ExternalLwaService : IExternalLwaService
{
    private readonly AuthenticationOptions _authOptions;
    private readonly HttpClient _httpClient;

    private string BeginSessionUrl => string.Concat(_authOptions.BaseUri, "beginSession");
    private string ExchangeCodeUrl => string.Concat(_authOptions.BaseUri, "exchangeLwaCode");
    private string LogoutUrl => string.Concat(_authOptions.BaseUri, "logout");
    
    public ExternalLwaService(
        IOptions<AuthenticationOptions> authOptions,
        HttpClient httpClient)
    {
        _authOptions = authOptions.Value;
        _httpClient = httpClient;
    }
    
    public async Task<string?> GetStateAsync()
    {
        var response = await _httpClient.PostAsync(BeginSessionUrl, null);
        var content = await response.Content.ReadFromJsonAsync<BeginSessionResponseDto>();
        
        var state = content?.Value?.State;
        if (string.IsNullOrWhiteSpace(state))
            throw new LwaException("Unable to retrieve session token");

        return state;
    }

    public async Task<string> ExchangeLwaCodeAsync(string state, string code)
    {
        var response = await _httpClient.PostAsJsonAsync(ExchangeCodeUrl, new { state = state, code = code });
        var content = await response.Content.ReadFromJsonAsync<ExchangeLwaCodeResponseDto>();

        var bearer = content?.Value.AccessToken;
        if (string.IsNullOrWhiteSpace(bearer))
            throw new LwaException("Unable to retrieve bearer");
        
        return bearer;
    }

    public async Task LogoutAsync()
    {
        await _httpClient.PostAsync(LogoutUrl, null);
    }
}