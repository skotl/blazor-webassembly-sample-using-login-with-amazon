namespace BlazorAuthApp.Auth;

public interface IExternalLwaService
{
    Task<string?> GetStateAsync();
    Task<string> ExchangeLwaCodeAsync(string state, string code);
    Task LogoutAsync();
}