using Blazored.LocalStorage;

namespace BlazorAuthApp.Auth;

public class BearerStorage : IBearerStorage
{
    private const string BearerSetting = "Authorization";
    private readonly ILocalStorageService _localStorageService;

    public BearerStorage(ILocalStorageService localStorageService)
    {
        _localStorageService = localStorageService;
    }
    
    public async Task<string> GetAsync()
    {
        return await _localStorageService.GetItemAsStringAsync(BearerSetting);
    }

    public async Task StoreAsync(string token)
    {
        if (string.IsNullOrWhiteSpace(token))
        {
            await ClearAsync();
        }
        else
        {
            await _localStorageService.SetItemAsStringAsync(BearerSetting, token);
        }
    }

    public async Task ClearAsync()
    {
        await _localStorageService.RemoveItemAsync(BearerSetting);
    }
}