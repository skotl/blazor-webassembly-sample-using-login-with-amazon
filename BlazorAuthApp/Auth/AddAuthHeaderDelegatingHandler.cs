using System.Net.Http.Headers;

namespace BlazorAuthApp.Auth;

public class AddAuthHeaderDelegatingHandler : DelegatingHandler
{
    private readonly IBearerStorage _bearerStorage;

    public AddAuthHeaderDelegatingHandler(IBearerStorage bearerStorage)
        : base(new HttpClientHandler())
    {
        _bearerStorage = bearerStorage;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        var bearer = await _bearerStorage.GetAsync();

        if (!string.IsNullOrWhiteSpace(bearer))
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", bearer);

        return await base.SendAsync(request, cancellationToken);
    }
}