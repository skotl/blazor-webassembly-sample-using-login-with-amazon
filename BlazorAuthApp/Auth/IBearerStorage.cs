namespace BlazorAuthApp.Auth;

public interface IBearerStorage
{
    Task<string> GetAsync();
    Task StoreAsync(string token);
    Task ClearAsync();
}