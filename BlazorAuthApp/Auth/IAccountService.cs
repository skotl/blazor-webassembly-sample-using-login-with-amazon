namespace BlazorAuthApp.Auth;

public interface IAccountService
{
    Task<bool> CompleteLogin(string state, string bearer);
    Task<bool> LogoutAsync();
}