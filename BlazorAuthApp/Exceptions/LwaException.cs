namespace BlazorAuthApp.Exceptions;

public class LwaException : Exception
{
    public LwaException(string msg) : base (msg)
    {}
}