namespace BlazorAuthApp.Model;

// ReSharper disable once ClassNeverInstantiated.Global
public class AmazonOptions
{
    public string ClientId { get; set; } = string.Empty;
}