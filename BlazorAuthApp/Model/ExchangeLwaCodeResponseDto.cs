using System.Text.Json.Serialization;

namespace BlazorAuthApp.Model;

// ReSharper disable once ClassNeverInstantiated.Global
public class ExchangeLwaCodeResponseDto
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ResponseValue
    {
        [JsonPropertyName("access_token")] 
        public string AccessToken { get; set; } = string.Empty;
    }

    public ResponseValue Value { get; set; } = new ResponseValue();
}