namespace BlazorAuthApp.Model;

// ReSharper disable once ClassNeverInstantiated.Global
public class BeginSessionResponseDto
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class StateValue
    {
        public string? State { get; set; }
    }

    public StateValue Value { get; set; } = new StateValue();
}