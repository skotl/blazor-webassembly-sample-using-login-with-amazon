using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using BlazorAuthApp;
using BlazorAuthApp.Auth;
using BlazorAuthApp.Model;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Options;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services
    .AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) })
    .AddScoped<AuthenticationStateProvider, CustomAuthenticationProvider>()
    .AddScoped<IBearerStorage, BearerStorage>()
    .AddScoped<IAccountService, AccountService>()
    .AddScoped<IExternalLwaService, ExternalLwaService>()
    .AddAuthorizationCore()
    .AddBlazoredLocalStorage()
    .Configure<AmazonOptions>(builder.Configuration.GetSection("Amazon"))
    .Configure<AuthenticationOptions>(builder.Configuration.GetSection("Authentication"))
    .AddScoped(sp => new HttpClient(new AddAuthHeaderDelegatingHandler(
        sp.GetRequiredService<IBearerStorage>())) 
    {
        BaseAddress = new Uri(sp.GetRequiredService<IOptions<AuthenticationOptions>>().Value.BaseUri)
    })
    ;


await builder.Build().RunAsync();
