# Blazor Web Assembly and Login-with-Amazon

This sample project shows how to use Login-with-Amazon within a Blazor WebAssembly application.
It may also be useful as a starter for any custom (non-OIDC / Azure) authentication solution within Blazor WASM.

The project is based on the standard `blazorwasm` dotnet template project, as created by:

```bash
dotnet new blazorwasm -au individual
```

*Note that the template was created in mid 2023 so if you're looking at this project
much later than then, you probably want to start with a current template and migrate
the changes from here. The first commit was straight after the template project was
created so you can use git to identify the changes between that commit and the present day.*


## Credits
The original setup of the `CustomAuthenticationProvider` and related services was
taken with thanks from 
https://www.learmoreseekmore.com/2020/10/blazor-webassembly-custom-authentication-from-scratch.html


## Getting started
Here's the easy bit - you're going to need the following nuget packages:

```
Blazored.LocalStorage
Microsoft.Extensions.Options.ConfigurationExtensions
```

You are also going to need your own backend API service to handle the
Authorization Code Grant with the Amazon services. See the [Amazon documentation](https://developer.amazon.com/docs/login-with-amazon/web-docs.html)
for more details.
This doesn't need to be particularly complex and, while the docs linked above should
be the most up to date I did find that a much [older guide](https://images-na.ssl-images-amazon.com/images/G/01/lwa/dev/docs/website-developer-guide._TTH_.pdf)
(from 2017!) was far more 
helpful.

## The back-end service

I don't include code for the back-end so you will have to roll your own. Refer to the
above Amazon docs for detailed information but, while there are many ways to manage
this, I describe my API endpoints here.

This can probably get you started (hint:
I'd start with just simple methods to return dummy data until you're ready to complete 
the end-to-end process with Amazon).

### Workflow

The back-end has two helpers for the login process and a single helper for logging out.
When we ask Amazon to log our user in we pass it a `state` value that is used to find
the original request in the back-end and then tie a session to it.

In my back-end app, this state is a one-time guid that is stored in a token table (I
remove tokens that are more than one hour old). When the user finally logs in to 
Amazon, they are given a code that we can exchange for a JWT structure. We use this
JWT to access the Amazon back-end APIs.

So, when the user starts to log in we:
1. Create a new token and pass that to LWA as a `state`
2. When the user logs in we get a `code` back from LWA
3. Our back-end verifies that the state token exists, then deletes it (one-time use!)
4. We exchange the code with LWA to get the JWT
5. Once we have the JWT we can fetch the user's Amazon profile
6. With the profile we check whether the user exists in our back-end system, creating it if not
7. A session is saved in the back end with the JWT and the user's ID
8. A guid (*not* the JWT!) is wrapped up with some claims and returned to the Blazor app

### Get state token
```csharp
// POST
// Anonymous
// Route = "beginSession
public async Task<IActionResult> BeginSession()
```

This returns HTTP 200 with a `{"state": "guid"}` state variable to be used during
the login process.

### Exchange LWA code
```csharp
// POST
// Anonymous
// Route = "exchangeLwaCode"
public async Task<IActionResult> ExchangeLwaCode(string state, string code)
```

This calls into LWA and exchanges the `code` for a JWT. It then uses the `state` to
build out a new session and eventually returns a bearer token (JWT) containing the
claims.

### Log out
```csharp
// POST
// Authenticated
// Route = "logout"
public async Task<IActionResult> Logout()
```

A call to this method requires the `Authorization` header to be set to the JWT as it
uses that to find the session, which it deletes (rendering the `Authorization`) 
bearer & JWT useless.

## App configuration

Create `appsettings.json` (ensure it's git-ignored), set to copy-always, and configure as below:

```json
{
  "Amazon": {
    "ClientId": "Your LWA client id"
  },
  "Authentication": {
    "BaseUri": "http://the base URI of your back-end auth service"
  }
}
```

## Login-with-Amazon configuration

Follow the Amazon dev docs to get an account, register for LWA and set up an
application within the LWA area. You *must* set up an origin that matches your Blazor
web-app's URL and this *must* be https.

If you get this stage wrong then you get weird errors about the redirect_uri not being
listed (or it's set to an odd amazon address).

It *needs* to be https and needs to match the base address of the calling app.
